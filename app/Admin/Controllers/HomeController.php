<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Xn\Admin\Controllers\Dashboard;
use Xn\Admin\Layout\Column;
use Xn\Admin\Layout\Content;
use Xn\Admin\Layout\Row;

class HomeController extends Controller
{
    public function index(Content $content)
    {
        return $content
            ->title('Dashboard')
            ->description('Description...')
            ->row(Dashboard::title())
            ->row(function (Row $row) {

                $row->column(4, function (Column $column) {
                    $column->append(Dashboard::environment());
                });

                $row->column(4, function (Column $column) {
                    $column->append(Dashboard::extensions());
                });

                $row->column(4, function (Column $column) {
                    $column->append(Dashboard::dependencies());
                });
            });
    }
}
