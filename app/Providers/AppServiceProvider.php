<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Xn\Admin\Auth\Session\AdminDatabaseSessionHandler;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
